function Card ({title, img}) {
return (
    <div className="card">
        <p>{title}</p>
        <img src={img}></img>
    </div>
)
}

export default Card