import Card from "./card"
import "./card.css"

function Cards({ data }) {
    const newArr = createValidArray(data)
    return (
        <div className="cards">
            {
                newArr ? newArr.map((e, i) => {
                    return <Card key={i * 10 + Math.random()} title={e.title} img={e.img}></Card>
                }) : newArr
            }
        </div>
    )
}

function createValidArray(oldArr) {
    if (Array.isArray(oldArr)) return null;

    let [...arr] = oldArr.results;

    arr = arr.map((obj) => {
        return {
            title: obj.name || obj.title,
            img: searchImg(obj.url)
        }
    })
    return arr
}

function searchImg(url) {
    let id = "";
    const urls = {
        people: "https://starwars-visualguide.com/assets/img/characters/",
        films: "https://starwars-visualguide.com/assets/img/films/",
        planets: "https://starwars-visualguide.com/assets/img/planets/",
        species: "https://starwars-visualguide.com/assets/img/species/",
        starships: "https://starwars-visualguide.com/assets/img/starships/",
        vehicles: "https://starwars-visualguide.com/assets/img/vehicles/"
    }
    if(typeof url === "string"){
        id = url.match(/\d+/).join();
    }else{
       throw new Error("url is not string")
    }
    if(url.includes("people")){
        return urls.people+id+".jpg"
    }else if(url.includes("films")){
        return urls.films+id+".jpg"
    }else if(url.includes("planets")){
        return urls.planets+id+".jpg"
    }else if(url.includes("species")){
        return urls.species+id+".jpg"
    }else if(url.includes("starships")){
        return urls.starships+id+".jpg"
    }else if(url.includes("vehicles")){
        return urls.vehicles+id+".jpg"
    }
}


export default Cards